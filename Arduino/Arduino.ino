// Waterflow
volatile int flow_frequency; // Measures flow sensor pulses
float vol, l_minute, overall_vol;
static const uint8_t SENSOR_PIN = 2; // Sensor Input
static const uint8_t KLAPAN_PIN = 3;

// Vars
String resp;
uint8_t i;
uint64_t currentTime, cloopTime;
static const uint64_t DELAY = 5000;

// MAX7219
#define MAX7219_DIN 7
#define MAX7219_CLK 8
#define MAX7219_CS  9

enum {  
  MAX7219_REG_DECODE    = 0x09,  
  MAX7219_REG_INTENSITY = 0x0A,
  MAX7219_REG_SCANLIMIT = 0x0B,
  MAX7219_REG_SHUTDOWN  = 0x0C,
  MAX7219_REG_DISPTEST  = 0x0F 
};

enum  { 
  OFF = 0,  
  ON  = 1 
};

const byte DP = 0b10000000;
const byte L  = 0b00000000;


// Interrupt function
void flow ()
{
  ++flow_frequency;
}

void setup()
{
  Serial.begin(9600);
  delay(10); // We start by connecting to a WiFi network
  
  pinMode(KLAPAN_PIN, OUTPUT);

  // Waterflow
  flow_frequency = 0;
  pinMode(SENSOR_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(SENSOR_PIN), flow, RISING); // Setup Interrupt

  cloopTime = millis();
  vol = overall_vol = 0;

  pinMode(MAX7219_DIN, OUTPUT);   // serial data-in
  pinMode(MAX7219_CS, OUTPUT);    // chip-select, active low    
  pinMode(MAX7219_CLK, OUTPUT);   // serial clock
  digitalWrite(MAX7219_CS, HIGH);

  displayReset();
}

void loop()
{
  currentTime = millis();

  // Every second, calculate and print litres/hour
  if (currentTime >= (cloopTime + DELAY))
  {
    cloopTime = currentTime; // Updates cloopTime

    read_flow(&l_minute, &vol);
    Serial.println("," + String(vol) + "," + String(l_minute));

    while (!Serial.available());

    resp = Serial.readString();
    i = resp.indexOf(",");

    if (resp.substring(0, i).toInt() == 200)
      vol = 0;
    
    
    if (resp.substring(i + 1).startsWith("1"))
    {
      Serial.println("HIGH");  
      digitalWrite(KLAPAN_PIN, HIGH);
    }
      
    else if (resp.substring(i + 1).startsWith("0"))
    {
      Serial.println("LOW");  
      digitalWrite(KLAPAN_PIN, LOW);
    }
  }
}

void read_flow(float *rate, float *vol)
{
  if (flow_frequency != 0)
  {
    // Pulse frequency (Hz) = 7.5Q, Q is flow rate in L/min.
    *rate = (flow_frequency / 7.5); // (Pulse frequency x 60 min) / 7.5Q = flowrate in L/hour

    *vol += (*rate / 60);
    overall_vol += *vol;
    flow_frequency = 0; // Reset Counter

  }

  if (overall_vol < 10)
    displayData("0" +  String(overall_vol));
  else 
    displayData(String(overall_vol));
}

 
// ... write a value into a max7219 register 
// See MAX7219 Datasheet, Table 1, page 6
void set_register(byte reg, byte value)  
{
    digitalWrite(MAX7219_CS, LOW);
    shiftOut(MAX7219_DIN, MAX7219_CLK, MSBFIRST, reg);
    shiftOut(MAX7219_DIN, MAX7219_CLK, MSBFIRST, value);
    digitalWrite(MAX7219_CS, HIGH);
}

void displayReset()
{
  set_register(MAX7219_REG_SHUTDOWN, OFF);   // turn off display
  set_register(MAX7219_REG_DISPTEST, OFF);   // turn off test mode
  set_register(MAX7219_REG_INTENSITY, 0x0D); // display intensity
}

void displayData(String data)  
{
    set_register(MAX7219_REG_SHUTDOWN, OFF);  // turn off display
    set_register(MAX7219_REG_SCANLIMIT, 7);   // scan limit 8 digits
    set_register(MAX7219_REG_DECODE, 0b11111111); // decode all digits

    set_register(1, -1);
    set_register(2, -1);
    set_register(3, -1);
    set_register(4, -1);
    set_register(5, data.charAt(4));
    set_register(6, data.charAt(3));
    set_register(7, data.charAt(1)| DP);
    set_register(8, data.charAt(0));


    set_register(MAX7219_REG_SHUTDOWN, ON);   // Turn on display
}
