#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

// WIFI
HTTPClient http;

const char *ssid = "AYLA";
const char *password = "Chessmaster10!";
const char *host = "http://test.sumaks.com/aquahackaton/data/"; // edit the host adress, ip address etc.

String mac;
String resp, data;
int httpCode;


void setup() {

  Serial.begin(9600);
  delay(10); // We start by connecting to a WiFi network

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default, would try to act as both a client and an access-point and could cause network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED)
    delay(500);
  
  WiFi.localIP());

  mac = WiFi.macAddress();
  mac.replace(":", "");
}

void loop() {

  if (Serial.available())
  {
    data = Serial.readString();
    send_data(mac + data);

    Serial.println(String(httpCode) + "," + resp); // send data to Arduino
  }
  
}

void send_data(String data)
{
  //Serial.print("connecting to ");
  //Serial.println(host); // Use WiFiClient class to create TCP connections

  http.begin(host);
  http.addHeader("Content-Type", "text/plain");

  //Serial.print("Sending Data: ");
  //Serial.println(data);
  httpCode = http.POST(data);

  if (httpCode > 0)
    resp = http.getString();   //Get the request response payload      

  http.end();
}
